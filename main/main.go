package main

import (
	"fmt"
)

func main() {
	var numberString string

	numberString = "4539 1488 0343 6467"
	fmt.Printf("Card number %s is %t\n", numberString, luhnValidation(numberString))

	numberString = "4"
	fmt.Printf("Card number %s is %t\n", numberString, luhnValidation(numberString))

	numberString = "4KKK3914880346467"
	fmt.Printf("Card number %s is %t\n", numberString, luhnValidation(numberString))

	numberString = "card"
	fmt.Printf("Card number %s is %t\n", numberString, luhnValidation(numberString))

	numberString = "8273 1232 7352 0569"
	fmt.Printf("Card number %s is %t\n", numberString, luhnValidation(numberString))

	numberString = "4539 14880343 64               67"
	fmt.Printf("Card number %s is %t\n", numberString, luhnValidation(numberString))

}
